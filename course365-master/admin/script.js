/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
"use strict";
$(document).ready(function () {


    const gBASE_URL = "https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1/";
    var gSTT = 1
    //  data table
    var gCourseDataTable = $("#courses-table").DataTable({
        columns: [
            { data: "id" },
            { data: "courseCode" },
            { data: "courseName" },
            { data: "level" },
            { data: "coverImage" },
            { data: "isPopular" },
            { data: "isTrending" },
            { data: "action" },
        ],
        columnDefs: [
            {
                target: 0,
                render: function () {
                    return gSTT++
                }
            },
            {
                target: 7,
                "defaultContent": `<i class="far fa-edit text-primary" id="btn-edit-course" data="id"  style="font-size: 20px; cursor: pointer;" data-toggle="tooltip" title="Edit Course"></i>&nbsp; 
                <i class="fas fa-trash-alt text-danger" id="btn-delete-course" style="font-size: 20px; cursor: pointer;" data-toggle="tooltip" title="Delete Course"></i>
                `
            },
            {
                target: 5,
                class: "text-center",
                render: renderColIsPopular
            },
            {
                target: 6,
                class: "text-center",
                render: renderColIsTrending
            },
            {
                target: 4,
                class: "text-center",
                render: renderCoursePhoto
            },
        ]
    });
    var gCourseId = "";


    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading()
    //thực thi nút Add course
    $(document).on("click", "#btn-add-course", function () {
        //    show modal
        $(".modal-add-course").modal("show");

    })
    //thực thi nút confirm add course trên modal
    $(".modal-add-course").on("click", "#btn-confirm-add-course", function () {
        onBtnConfirmAddCourseClick();
    });
    //thực thi nút ấn edit course
    $(document).on("click", "#btn-edit-course", function () {
        onBtnEditCourseClick(this)
    });
    //thực thi nút confirm add course trên modal
    $(".modal-edit-course").on("click", "#btn-confirm-edit-course", function () {
        onBtnConfirmEditCourseClick();
    });

     //thực thi nút ấn delete course
     $("#courses-table").on("click", "#btn-delete-course", function () {
        onBtnDeleteCourseClick(this)
    });
    //thực thi nút confirm delete course trên modal
    $("#delete-confirm-modal").on("click", "#btn-confirm-delete-course", function () {
        onBtnConfirmDeleteCourseClick();
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        callApiToGetCourseList()

    }
    //hàm gọi api to get all course
    function callApiToGetCourseList() {
        $.ajax({
            url: gBASE_URL + "/courses",
            type: "GET",
            success: function (paramRes) {
                console.log(paramRes);
                loadDataToTable(paramRes)
            },
            error: function (paramError) {
                console.log(paramError.responseText)
            }
        })
    };
    //hàm ấn nút edit course
    function onBtnEditCourseClick(paramBtn) {
        var vCourseData = getCourseData(paramBtn);
        console.log(vCourseData);
        gCourseId = vCourseData.id;
        console.log(gCourseId)
        loadCourseDataToModal(vCourseData);
        $(".modal-edit-course").modal("show");
    }
    //hàm ấn nút delete course
    function onBtnDeleteCourseClick(paramBtn) {
        var vTableRow = $(paramBtn).parents("tr");
        var vDataTableRow = gCourseDataTable.row(vTableRow);
        var vCourseData = vDataTableRow.data()
        gCourseId = vCourseData.id;
        $("#delete-confirm-modal").modal("show");
    }
    //hàm ấn nút confirm add course trên modal:
    function onBtnConfirmAddCourseClick() {
        var vCourseDataRequestObj = {
            courseCode: "",
            courseName: "",
            price: 0,
            discountPrice: 0,
            duration: "",
            level: "",
            coverImage: "",
            teacherName: "",
            teacherPhoto: "",
            isPopular: false,
            isTrending: false
        };
        //B1: thu thập dữ liệu
        readData(vCourseDataRequestObj)
        console.log(vCourseDataRequestObj)
        //B2: validate
        var vDataValid = validateData(vCourseDataRequestObj)
        if (vDataValid) {
            //B3: call Api to add course
            callApiToCreateNewCourse(vCourseDataRequestObj);

        }
        //B4: handle add course
    };

    //hàm ấn nút confirm edit course trên modal:

    function onBtnConfirmEditCourseClick(paramCourseData) {
        var vCourseDataEditObj = {
            courseCode: "",
            courseName: "",
            price: 0,
            discountPrice: 0,
            duration: "",
            level: "",
            coverImage: "",
            teacherName: "",
            teacherPhoto: "",
            isPopular: false,
            isTrending: false
        };
        //B1: thu thập dữ liệu
        readDataEditCourse(vCourseDataEditObj)
        console.log(vCourseDataEditObj)
        //B2: validate
        var vDataEditValid = validateDataEdit(vCourseDataEditObj)
        if (vDataEditValid) {
            //B3: call Api to add course
            callApiToEditCourse(vCourseDataEditObj);

        }


        //B4: handle add course
    };
    //hàm ấn nút confirm delete course trên modal: 
    function onBtnConfirmDeleteCourseClick() {
        console.log("xoa xoa")
        $.ajax({
            url: gBASE_URL + "/courses/" + gCourseId,
            type: "DELETE",
            success: function (paramRes) {
                console.log(paramRes);
                handleCourseDeleted()
            },
            error: function (paramError) {
                console.log(paramError);

            }

        });
    };



    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //hàm load course data to table
    function loadDataToTable(paramData) {
        gCourseDataTable.rows.add(paramData);
        gCourseDataTable.draw();

    }
    function renderColIsPopular(paramData) {
        if (paramData == true) {
            return `<i class="far fa-check-square text-success" style="font-size: 20px;"></i>`
        } else {
            return `<i class="far fa-square" style="font-size: 20px;"></i></i>`
        }
    }
    function renderColIsTrending(paramData) {
        if (paramData == true) {
            return `<i class="far fa-check-square text-success" style="font-size: 20px;"></i>`
        } else {
            return `<i class="far fa-square" style="font-size: 20px;"></i></i>`
        }
    }
    //hàm render course photo:
    function renderCoursePhoto(paramData) {
        return `<img src="${paramData}" alt="" width="100px">
        `
    }

    // vùng chứa hàm xử lý add course
    //B1: thu thập dữ liệu
    function readData(paramCourseData) {
        paramCourseData.courseCode = $("#inp-course-code").val().trim();
        paramCourseData.courseName = $("#inp-course-name").val().trim();
        paramCourseData.price = parseInt($("#inp-course-price").val().trim());
        paramCourseData.discountPrice = parseInt($("#inp-course-price").val().trim());
        paramCourseData.duration = $("#inp-course-duration").val().trim();
        paramCourseData.level = $("#select-course-level").val();
        paramCourseData.coverImage = $("#inp-course-photo").val().trim();
        paramCourseData.teacherName = $("#inp-teacher-name").val().trim();
        paramCourseData.teacherPhoto = $("#inp-teacher-photo").val().trim();
        paramCourseData.isPopular = $("#select-is-popular").val();
        paramCourseData.isTrending = $("#select-is-trending").val();
    };
    //B2: validate
    function validateData(paramCourseData) {
        if (paramCourseData.courseCode.length < 10) {
            alert("Mã khóa học phải chứa ít nhất 10 ký tự")
            return false
        }
        if (paramCourseData.courseName.length < 10) {
            alert("Tên khóa học phải chứa ít nhất 20 ký tự")
            return false
        }
        if (isNaN(paramCourseData.price)) {
            alert("Giá khóa học phải là số")
            return false
        }
        if (paramCourseData.duration == "") {
            alert("Hãy nhập độ dài khóa học")
            return false
        }
        if (paramCourseData.level == 0) {
            alert("Hãy chọn trình độ khóa học")
            return false
        }
        if (paramCourseData.coverImage == "") {
            alert("Hãy nhập link ảnh khóa học")
            return false
        }
        if (paramCourseData.teacherName == "") {
            alert("Hãy nhập tên giáo viên")
            return false
        }
        if (paramCourseData.teacherPhoto == "") {
            alert("Hãy nhập link ảnh giáo viên")
            return false
        }
        if (paramCourseData.isPopular == 0) {
            alert("Khóa học có phổ biến không?")
            return false
        }
        if (paramCourseData.isTrending == 0) {
            alert("Khóa học có đang nổi không?")
            return false
        }
        return true
    };

    //B3: call Api to add course
    function callApiToCreateNewCourse(paramCourseData) {
        $.ajax({
            url: gBASE_URL + "/courses",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(paramCourseData),
            success: function (paramRes) {
                console.log(paramRes);
                handleCourseAdded()
            },
            error: function (paramError) {
                console.log(paramError);

            }

        })
    };
    //B4: handle course added:
    function handleCourseAdded() {
        alert("Tạo mới khóa học thành công!");
        $(".modal-add-course").modal("hide");
        location.reload();
    }

    //vùng chứa hàm xử lý edit course:
    function getCourseData(paramBtn) {
        var vTableRow = $(paramBtn).parents("tr");
        var vDataTableRow = gCourseDataTable.row(vTableRow);
        var vCourseData = vDataTableRow.data()
        return vCourseData
    };
    //hàm load course data to modal
    function loadCourseDataToModal(paramCourseData) {
        $("#inp-edit-course-code").val(paramCourseData.courseCode)
        $("#inp-edit-course-name").val(paramCourseData.courseName)
        $("#inp-edit-course-price").val(paramCourseData.price)
        $("#inp-edit-course-duration").val(paramCourseData.duration)
        $("#select-edit-course-level").val(paramCourseData.level)
        $("#inp-edit-course-photo").val(paramCourseData.coverImage)
        $("#inp-edit-teacher-name").val(paramCourseData.teacherName)
        $("#inp-edit-teacher-photo").val(paramCourseData.teacherPhoto)
        $("#select-edit-is-popular").val(paramCourseData.isPopular)
        $("#select-edit-is-trending").val(paramCourseData.isTrending)
    }
    //hàm đọc dữ liệu edit data edit course
    function readDataEditCourse(ParamData) {
        ParamData.courseCode = $("#inp-edit-course-code").val().trim();
        ParamData.courseName = $("#inp-edit-course-name").val().trim();
        ParamData.price = parseInt($("#inp-edit-course-price").val().trim());
        ParamData.discountPrice = parseInt($("#inp-edit-course-price").val().trim());
        ParamData.duration = $("#inp-edit-course-duration").val().trim();
        ParamData.level = $("#select-edit-course-level").val();
        ParamData.coverImage = $("#inp-edit-course-photo").val().trim();
        ParamData.teacherName = $("#inp-edit-teacher-name").val().trim();
        ParamData.teacherPhoto = $("#inp-edit-teacher-photo").val().trim();
        ParamData.isPopular = $("#select-edit-is-popular").val();
        ParamData.isTrending = $("#select-edit-is-trending").val();
    };
    //hàm validate data edit
    function validateDataEdit(paramData) {
        if (paramData.courseCode.length < 10) {
            alert("Mã khóa học phải chứa ít nhất 10 ký tự")
            return false
        }
        if (paramData.courseName.length < 10) {
            alert("Tên khóa học phải chứa ít nhất 20 ký tự")
            return false
        }
        if (isNaN(paramData.price)) {
            alert("Giá khóa học phải là số")
            return false
        }
        if (paramData.duration == "") {
            alert("Hãy nhập độ dài khóa học")
            return false
        }
        if (paramData.level == 0) {
            alert("Hãy chọn trình độ khóa học")
            return false
        }
        if (paramData.coverImage == "") {
            alert("Hãy nhập link ảnh khóa học")
            return false
        }
        if (paramData.teacherName == "") {
            alert("Hãy nhập tên giáo viên")
            return false
        }
        if (paramData.teacherPhoto == "") {
            alert("Hãy nhập link ảnh giáo viên")
            return false
        }
        if (paramData.isPopular == 0) {
            alert("Khóa học có phổ biến không?")
            return false
        }
        if (paramData.isTrending == 0) {
            alert("Khóa học có đang nổi không?")
            return false
        }
        return true
    };
    //hàm gọi Api to edit course:
    function callApiToEditCourse(paramData) {
        $.ajax({
            url: gBASE_URL + "/courses/" + gCourseId,
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(paramData),
            success: function (paramRes) {
                console.log(paramRes);
                handleCourseEdited()
            },
            error: function (paramError) {
                console.log(paramError);

            }

        });
    };
    //B4: handle course added:
    function handleCourseEdited() {
        alert("Sửa khóa học thành công!");
        $(".modal-edit-course").modal("hide");
        location.reload();
    }

    //vùng chứa hàm xử lý delete course:
    function handleCourseDeleted() {
        alert("Xóa khóa học thành công!");
        $("#delete-confirm-modal").modal("hide");
        location.reload();
    }
});