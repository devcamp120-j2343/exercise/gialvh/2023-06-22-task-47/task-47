/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 9,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Intermediate",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 10,
            courseCode: "FE_UIUX_COURSE_211",
            courseName: "Thinkful UX/UI Design Bootcamp",
            price: 950,
            discountPrice: 700,
            duration: "5h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-uiux.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: false,
            isTrending: false
        },
        {
            id: 11,
            courseCode: "FE_WEB_REACRJS_210",
            courseName: "Front-End Web Development with ReactJs",
            price: 1100,
            discountPrice: 850,
            duration: "6h 20m",
            level: "Advanced",
            coverImage: "images/courses/course-reactjs.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 12,
            courseCode: "FE_WEB_BOOTSTRAP_101",
            courseName: "Bootstrap 4 Crash Course | Website Build & Deploy",
            price: 750,
            discountPrice: 600,
            duration: "3h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-bootstrap.png",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 14,
            courseCode: "FE_WEB_RUBYONRAILS_310",
            courseName: "The Complete Ruby on Rails Developer Course",
            price: 2050,
            discountPrice: 1450,
            duration: "8h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-rubyonrails.png",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        }
    ]
};
const gBASE_URL = "https://630890e4722029d9ddd245bc.mockapi.io/api/v1";
var gCourseList = [];
var gPopularCourseList = [];
var gTrendingCourseList = [];
const gTRUE = true;


/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();

});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    callApiToGetCourseList();
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function callApiToGetCourseList() {
    $.ajax({
        url: gBASE_URL + "/courses",
        type: "GET",
        success: function (paramRes) {
            gCourseList = paramRes;
            console.log(gCourseList);
            filterPopularCourse();
         console.log(gPopularCourseList)

            filterTrendingCourse()
             console.log(gTrendingCourseList)



        },
        error: function (paramError) {
            console.log(paramError)
        }
    })
};

function filterPopularCourse() {
   for (var bI = 0; bI < gCourseList.length; bI++) {
        if (gCourseList[bI].isPopular == true) {
            gPopularCourseList.push(gCourseList[bI]);
            loadPopularCourse()
        }
   }
};

function filterTrendingCourse() {
    for (var bI = 0; bI < gCourseList.length; bI++) {
         if (gCourseList[bI].isTrending == true) {
            gTrendingCourseList.push(gCourseList[bI]);
            loadTrendingCourse()
         }
    }
 };

 function loadPopularCourse() {
    $("#div-popular-course").html("");
    for (var bI = 2; bI < gPopularCourseList.length; bI++) {
        $("#div-popular-course").append(`
        <div class="card mt-3" style="width: 18rem;">
            <img class="card-img-top" src="${gPopularCourseList[bI].coverImage}" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title text-primary" style="font-size: x-large;">${gPopularCourseList[bI].courseName}</h5>
              <div class="row ml-1">
                <p class="card-text" style="font-weight: 600; font-size: large;"><i class="far fa-clock"></i> ${gPopularCourseList[bI].duration}</p>
                <p class="ml-3" style="font-weight: 600; font-size: large;">${gPopularCourseList[bI].level}</p>
              </div>
              <div class="row ml-1">
                <h4 style="font-weight: 700;">$${gPopularCourseList[bI].discountPrice}</h4>
                <h4 class="ml-1" style="font-weight: 500; color: lightgray; text-decoration:line-through;">$${gPopularCourseList[bI].price}</h4>
              </div>
            </div>
            <div class="card-footer">
              <img src="${gPopularCourseList[bI].teacherPhoto}" alt="" width="60px" style="border-radius: 50%;">
              <p class="mt-1 mr-5" style="font-weight:500; font-size: medium;">${gPopularCourseList[bI].teacherName}</p>
              <i class="far fa-bookmark mb-3" style="font-weight:500; font-size: larger;"></i>
            </div>
          </div>
        `)
    }
 };

 function loadTrendingCourse() {
    $("#div-trending-course").html("");
    for (var bI = 2; bI <gTrendingCourseList.length; bI++) {
        $("#div-trending-course").append(`
        <div class="card mt-3" style="width: 18rem;">
            <img class="card-img-top" src="${gTrendingCourseList[bI].coverImage}" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title text-primary" style="font-size: x-large;">${gTrendingCourseList[bI].courseName}</h5>
              <div class="row ml-1">
                <p class="card-text" style="font-weight: 600; font-size: large;"><i class="far fa-clock"></i> ${gTrendingCourseList[bI].duration}</p>
                <p class="ml-3" style="font-weight: 600; font-size: large;">${gTrendingCourseList[bI].level}</p>
              </div>
              <div class="row ml-1">
                <h4 style="font-weight: 700;">$${gTrendingCourseList[bI].discountPrice}</h4>
                <h4 class="ml-1" style="font-weight: 500; color: lightgray; text-decoration:line-through;">$${gTrendingCourseList[bI].price}</h4>
              </div>
            </div>
            <div class="card-footer">
              <img src="${gTrendingCourseList[bI].teacherPhoto}" alt="" width="60px" style="border-radius: 50%;">
              <p class="mt-1 mr-5" style="font-weight:500; font-size: medium;">${gTrendingCourseList[bI].teacherName}</p>
              <i class="far fa-bookmark mb-3" style="font-weight:500; font-size: larger;"></i>
            </div>
          </div>
        `)
    }
 };








